
class SimpleCalculator:
    """
    this the class simple calculator to calcuate sum substract multiply and divide
    """

    def __init__(self, a, b):
        """
        this is the initiation function
        """
        self.chiffre1 = a
        self.chiffre2 = b

    def sum(self):
        """
        this is the sum function
        """
        print(self.chiffre1, "+", self.chiffre2, "=")
        res = self.chiffre1 + self.chiffre2
        return res

    def substract(self):
        """
        this is the substract function
        """
        print(self.chiffre1, "-", self.chiffre2, "=")
        res = self.chiffre1 - self.chiffre2
        return res

    def multiply(self):
        """
        this is the multiply function
        """
        print(self.chiffre1, "*", self.chiffre2, "=")
        res = self.chiffre1 * self.chiffre2
        return res

    def divide(self):
        """
        this is the divide function
        """
        print(self.chiffre1, "/", self.chiffre2, "=")
        res = self.chiffre1 / self.chiffre2
        return res

